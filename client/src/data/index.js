export const todos = [
  {
    _id: '1',
    name: 'Mexicana',
    desc: 'Delicious hot pizza in mexican style.',
    price: {
      us: 10,
      eu: 8,
    },
    ingredients: [
      'Meat',
      'Tomatoes',
      'Chili',
      'Jalapeno',
    ],
    tags: [
      'Hot',
      'Meat',
    ],
    image: 'https://images.unsplash.com/photo-1551978129-b73f45d132eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1004&q=80'
  },
  {
    _id: '2',
    name: 'Vegetarian',
    image: 'https://images.unsplash.com/photo-1581873372796-635b67ca2008?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
    price: {
      us: 10.5,
      eu: 8.5,
    },
    tags: [
      'Veggi'
    ]
  },
  {
    _id: '3',
    name: 'Carbonara',
    image: 'https://images.unsplash.com/photo-1581873372796-635b67ca2008?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
    price: {
      us: 12,
      eu: 9,
    },
    tags: [
      'Meat'
    ]
  },
  {
    _id: '4',
    name: 'Margareta',
    tags: [],
    price: {
      us: 6,
      eu: 4,
    },
    image: 'https://images.unsplash.com/photo-1513104890138-7c749659a591?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
  },
  {
    _id: '5',
    name: 'Hawaii',
    image: 'https://images.unsplash.com/photo-1581873372796-635b67ca2008?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
    price: {
      us: 10,
      eu: 8,
    },
    tags: [
      'Seafood'
    ]
  },
  {
    _id: '6',
    name: 'Village',
    price: {
      us: 10,
      eu: 8,
    },
    image: 'https://images.unsplash.com/photo-1581873372796-635b67ca2008?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
    tags: [
      'Meat'
    ]
  },
]

