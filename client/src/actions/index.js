import axios from 'axios';
import {todos as TODOS} from '../data/index';
const isDev = process.env.NODE_ENV!=='production';
const serverUrl = isDev ? 'http://localhost:5000' : 'https://test-task-pizza.herokuapp.com';


export const addFakeTodos = async () => {
  localStorage.setItem('TODOS',JSON.stringify(TODOS));
  return;
}


export const getTodos = async () => {
  let todos = localStorage.getItem('TODOS');
  todos = todos && JSON.parse(todos);
  return todos;
}


export const addTodo = async (todo) => {
  try {
    let todos = localStorage.getItem('TODOS');
    todos = todos && JSON.parse(todos);
    todos.push(todo)
    localStorage.setItem('TODOS',JSON.stringify(todos));
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}


export const editTodo = async (_id,todo) => {
  try {
    let todos = localStorage.getItem('TODOS');
    todos = todos && JSON.parse(todos);
    const index = todos.findIndex((todo) => todo._id===_id);
    todos[index] = todo;
    console.log('editTodo',todos)
    localStorage.setItem('TODOS',JSON.stringify(todos));
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}


export const delTodo = async (_id) => {
  try {
    let todos = localStorage.getItem('TODOS');
    todos = todos && JSON.parse(todos);
    todos = todos.filter((todo) => todo._id!==_id);
    localStorage.setItem('TODOS',JSON.stringify(todos));
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}

// =========================================

export const reg = async (body) => {
  try {
    const request = await fetch(`${serverUrl}/users/reg`,{
      // headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      headers: { 'X-Requested-With':'XMLHttpRequest' },
      method: 'POST',
      body: JSON.stringify(body),
      credentials: 'include',
    })
    const result = await request.json();
    console.log('reg',result)
    return result;
  } catch (e) {
    console.error(e);
    return false;
  }
}


export const login = async (body) => {
  try {
    const request = await axios.post(`${serverUrl}/users/login`, body, {
      // headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      headers: { 'X-Requested-With':'XMLHttpRequest' },
      withCredentials: true,
      responseType: 'json',
    })
    // const request = await fetch(`${serverUrl}/users/login`,{
    //   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    //   method: 'POST',
    //   body: JSON.stringify(body),
    //   credentials: 'include',
    // })
    // const result = await request.json();
    console.log('login',request.data)
    return request.data;
  } catch (e) {
    console.error(e);
    return false;
  }
}

export const logout = async (body) => {
  try {
    const request = await axios.post(`${serverUrl}/users/logout`, body, {
      // headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      headers: { 'X-Requested-With':'XMLHttpRequest' },
      withCredentials: true,
      responseType: 'json',
    })
    // const request = await fetch(`${serverUrl}/users/logout`,{
    //   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    //   method: 'POST',
    //   body: JSON.stringify(body),
    //   credentials: 'include',
    // })
    const result = request.data;
    const status = request.status;
    console.log('logout',{status,result})
    return {status,result};
  } catch (e) {
    console.error(e);
    return false;
  }
}


export const isLogged = async (body) => {
  try {
    const request = await axios.post(`${serverUrl}/users/isLogged`, body, {
      headers: { 'X-Requested-With':'XMLHttpRequest' },
      withCredentials: true,
      responseType: 'json',
    })
    // const request = await fetch(`${serverUrl}/users/isLogged`,{
    //   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    //   method: 'POST',
    //   body: JSON.stringify(body),
    //   credentials: 'include',
    //   mode: 'cors',
    // })
    // const result = await request.json();
    console.log('isLogged',request.data)
    return request.data;
  } catch (e) {
    console.error(e);
    return false;
  }
}
