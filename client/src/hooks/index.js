import useLocalStorage from './useLocalStorage';
import useGlobal from './useGlobal';


export {
  useLocalStorage,
  useGlobal,
}