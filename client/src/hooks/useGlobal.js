import React from "react";
import useGlobalHook from "use-global-hook";
import {initialState} from "../state";

const actions = {
  setState: (store, newState) => {
    store.setState({...store.state,...newState});
  },
};

const useGlobal = useGlobalHook(React, initialState, actions);
export default useGlobal;
