import React from 'react';
import {Switch, Route, Link} from "react-router-dom";
import * as Components from '../';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
}));


function App() {
  const c = useStyles();
  return (
    <div className={c.root}>

      <Components.MenuMain/>

      <Switch>
        <Route path="/cart">
          <Components.PageCart />
        </Route>
        <Route path="/orders">
          <Components.PageOrders />
        </Route>
        <Route path="/sign">
          <Components.PageSign />
        </Route>
        <Route path="/">
          <Components.PageMain />
        </Route>
        <Route path="*">
          <Components.NotFound />
        </Route>
      </Switch>

    </div>
  );
}

export default App;
