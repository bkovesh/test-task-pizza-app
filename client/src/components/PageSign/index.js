import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {useCookies} from 'react-cookie';
import {todos as TODOS} from '../../data/index';
import * as Actions from '../../actions';
import * as Hooks from '../../hooks';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import Chip from '@material-ui/core/Chip';
import Button from "@material-ui/core/Button";
import {Route, Switch, Link, useRouteMatch, useLocation} from "react-router-dom";
import * as Components from "../index";
import {Helmet} from "react-helmet";


const imageProduct = 'https://images.unsplash.com/photo-1551978129-b73f45d132eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1004&q=80';


const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 70,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  grid: {
    flexGrow: 1,
    margin: 20,
  },
  paper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: 320,
    width: 260,
    // padding: '10px 10px 5px 10px',
  },
  textArea: {
    width:'calc(100% - 20px)',
    padding: 10,
  },
  textField: {
    margin: 10,
  },
  formLogin: {
    padding: 20,
    marginTop: 70,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  containerSearch: {
    // marginTop: 20,
  },
  containerLabels: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  containerFilter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  containerFilterLabel: {
    marginRight: 10,
  },
  containerBtns: {
    position: 'relative',
  },
  formControl: {
    marginTop: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  btn: {
    margin: 2,
    boxShadow: 'none',
  },
  btnLogin: {
    margin: 10,
  },
  btnIcon: {
    width: 30,
    marginLeft: 5,
    marginRight: 5,
  },
  btnFav: {
    width: 40,
    minWidth: 40,
    position: 'absolute',
    right: 0,
  },
  label: {
    margin: 2,
  },
  productImage: {
    height: 140,
    width: '100%',
    objectFit: 'cover',
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  productDesc: {
    height: 40,
    marginLeft: 20,
    marginRight: 20,
    color: '#777777',
  },
  h1: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  m10: {
    margin: 10,
  },
}));




function MyHelmet(props) {
  const {title,desc} = props;
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{title}</title>
      <meta name="description" content={desc} />
      <link rel="canonical" href="http://mysite.com/example" />
    </Helmet>
  )
}




function FormReg(props) {
  const c = useStyles();
  const [cookies, setCookie, removeCookie] = useCookies(['cookie-name']);

  let [email, setEmail] = React.useState('');
  let [password, setPassword] = React.useState('');
  // console.log('FormReg',cookies)


  const handleSubmit = async () => {
    console.log('handleSubmit',{email,password})
    await Actions.reg({email,password})
  }


  return (
    <Paper className={c.formLogin}>

      <MyHelmet
      title='Pizza Delivery. Sign up.'
      desc='You can buy pizza here'
      />


      <div className={c.h1}>
        Sign up
      </div>
      <div className={c.m10}>
        <Link
        to={`./login`}
        >
          Already registered?
        </Link>
      </div>


      <TextField
      className={c.textField}
      id="outlined-basic"
      label="email"
      variant="outlined"
      onChange={(e) => setEmail(e.target.value)}
      value={email}
      />

      <TextField
      className={c.textField}
      id="outlined-basic"
      label="password"
      variant="outlined"
      onChange={(e) => setPassword(e.target.value)}
      value={password}
      />

      <Button
      className={c.btnLogin}
      onClick={handleSubmit}
      >
        Submit
      </Button>

    </Paper>
  );
}



function FormLogin(props) {
  const c = useStyles();
  const [cookies, setCookie, removeCookie] = useCookies(['connect.sid']);

  let [email, setEmail] = React.useState('');
  let [password, setPassword] = React.useState('');
  // console.log('FormLogin',cookies)


  const handleSubmit = async () => {
    console.log('handleSubmit',{email,password})
    await Actions.login({email,password})
  }


  return (
    <Paper className={c.formLogin}>

      <MyHelmet
      title='Pizza Delivery. Register.'
      desc='You can buy pizza here'
      />


      <div className={c.h1}>
        Log in
      </div>
      <div className={c.m10}>
        <Link
        to={`./reg`}
        >
          Not registered?
        </Link>
      </div>


      <TextField
      className={c.textField}
      id="outlined-basic"
      label="email"
      variant="outlined"
      onChange={(e) => setEmail(e.target.value)}
      value={email}
      />

      <TextField
      className={c.textField}
      id="outlined-basic"
      label="password"
      variant="outlined"
      onChange={(e) => setPassword(e.target.value)}
      value={password}
      />

      <Button
      className={c.btnLogin}
      onClick={handleSubmit}
      >
        Submit
      </Button>

    </Paper>
  );
}



function PageSign() {
  const c = useStyles();
  let { path, url } = useRouteMatch();
  let location = useLocation();

  let [todos, setTodos] = React.useState([]);

  let [todosPinned, setTodosPinned] = Hooks.useLocalStorage('todosPinned',[]);



  useEffect(() => {
    (async () => {
      console.log('PageSign',location)
    })()
  },[])





  return (
    <Container maxWidth="md" className={c.root}>



      <Switch>
        <Route path={`${path}/reg`}>
          <FormReg />
        </Route>
        <Route path={`${path}/login`}>
          <FormLogin />
        </Route>
      </Switch>


    </Container>
  );
}

export default PageSign;
