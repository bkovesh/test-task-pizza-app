import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {todos as TODOS} from '../../data/index';
import * as Actions from '../../actions';
import * as Hooks from '../../hooks';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import Chip from '@material-ui/core/Chip';
import Button from "@material-ui/core/Button";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import AddIcon from '@material-ui/icons/Add';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";


const imageProduct = 'https://images.unsplash.com/photo-1551978129-b73f45d132eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1004&q=80';


const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 70,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  grid: {
    flexGrow: 1,
    margin: 20,
  },
  paper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: 320,
    width: 260,
    // padding: '10px 10px 5px 10px',
  },
  textArea: {
    width:'calc(100% - 20px)',
    padding: 10,
  },
  containerSearch: {
    // marginTop: 20,
  },
  containerLabels: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  containerFilter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  containerFilterLabel: {
    marginRight: 10,
  },
  containerBtns: {
    position: 'relative',
  },
  containerFinalPrice: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
  },
  formControl: {
    marginTop: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  btn: {
    margin: 2,
    boxShadow: 'none',
  },
  btnIcon: {
    width: 30,
    marginLeft: 5,
    marginRight: 5,
  },
  btnFav: {
    width: 40,
    minWidth: 40,
    position: 'absolute',
    right: 0,
  },
  label: {
    margin: 2,
  },
  productImage: {
    height: 140,
    width: '100%',
    objectFit: 'cover',
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  productDesc: {
    height: 40,
    marginLeft: 20,
    marginRight: 20,
    color: '#777777',
  },
  header1: {
    fontWeight: 'bold',
    fontSize: 30,
    margin:20,
  },
  containerOrder: {
    margin:20,
  },
}));





function PageCart() {
  const c = useStyles();

  let [todos, setTodos] = React.useState([]);

  let [todosPinned, setTodosPinned] = Hooks.useLocalStorage('todosPinned',[]);

  let [pizzasAddedGlobal, setPizzasAddedGlobal] = Hooks.useGlobal((s) => s.pizzasAdded,(a) => a.setState);


  let finalPrice = {eu:0,us:0}
  let todosById = {}
  todos.map((todo) => {
    todosById[todo._id] = todo;
  })
  Object.keys(pizzasAddedGlobal).map((_id) => {
    if (!todosById[_id]) return;
    if (!todosById[_id].price) return;
    finalPrice.eu += pizzasAddedGlobal[_id] * todosById[_id].price.eu;
    finalPrice.us += pizzasAddedGlobal[_id] * todosById[_id].price.us;
  })


  useEffect(() => {
    (async () => {
      await Actions.addFakeTodos()
      await handleGetTodos()
    })()
  },[])


  const handleGetTodos = async () => {
    let newTodos = await Actions.getTodos()
    console.log('handleGetTodos',newTodos)
    // newTodos = newTodos.reverse();
    setTodos(newTodos)
  }

  const handlePin = async (_id) => {
    let newTodosPinned = [];
    if (todosPinned.includes(_id)) {
      newTodosPinned = todosPinned.filter((item) => item!==_id);
    } else {
      newTodosPinned = [...todosPinned,...[_id]]
    }
    setTodosPinned(newTodosPinned);
  };





  return (
    <Container maxWidth="md" className={c.root}>


      <div className={c.header1}>
        Your order
      </div>


      <Paper>
        <Table className={c.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="center">Count</TableCell>
              <TableCell align="center">Price, Euro</TableCell>
              <TableCell align="center">Price, Dollar</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { todos.map((todo) => {
              const {_id, name, desc, price} = todo;
              if (pizzasAddedGlobal && !Object.keys(pizzasAddedGlobal).includes(_id)) return;
              const count = pizzasAddedGlobal[_id] ? pizzasAddedGlobal[_id] : 0;
              if (count===0) return;
              return(
              <TableRow key={`todo-${_id}`}>
                <TableCell component="th" scope="row">
                  <b>{name}</b>
                </TableCell>
                <TableCell align="center">
                  <b>{count}</b>
                </TableCell>
                <TableCell align="center">
                  <b>{price && price.eu && price.eu * count}</b>
                </TableCell>
                <TableCell align="center">
                  <b>{price && price.us && price.us * count}</b>
                </TableCell>
              </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </Paper>



      <div className={c.containerFinalPrice}>
        <div>
          Final price, EUR:{' '}{finalPrice.eu}
        </div>
        <div>
          Final price, USD:{' '}{finalPrice.us}
        </div>
      </div>


      <div className={c.containerOrder}>
        <Button
        className={c.btn}
        variant="contained"
        color="secondary"
        elevation={0}
        // onClick={() => handleAdd(_id,1)}
        >
          Order
        </Button>
      </div>


    </Container>
  );
}

export default PageCart;
