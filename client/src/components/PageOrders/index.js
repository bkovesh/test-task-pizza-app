import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {todos as TODOS} from '../../data/index';
import * as Actions from '../../actions';
import * as Hooks from '../../hooks';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


const imageProduct = 'https://images.unsplash.com/photo-1551978129-b73f45d132eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1004&q=80';


const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 70,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  grid: {
    flexGrow: 1,
    margin: 20,
  },
  paper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: 320,
    width: 260,
    // padding: '10px 10px 5px 10px',
  },
  textArea: {
    width:'calc(100% - 20px)',
    padding: 10,
  },
  containerSearch: {
    // marginTop: 20,
  },
  containerLabels: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  containerFilter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  containerFilterLabel: {
    marginRight: 10,
  },
  containerBtns: {
    position: 'relative',
  },
  formControl: {
    marginTop: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  btn: {
    margin: 2,
    boxShadow: 'none',
  },
  btnIcon: {
    width: 30,
    marginLeft: 5,
    marginRight: 5,
  },
  btnFav: {
    width: 40,
    minWidth: 40,
    position: 'absolute',
    right: 0,
  },
  label: {
    margin: 2,
  },
  productImage: {
    height: 140,
    width: '100%',
    objectFit: 'cover',
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  productDesc: {
    height: 40,
    marginLeft: 20,
    marginRight: 20,
    color: '#777777',
  },
  h1: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: 10,
  },
}));





function PageOrders() {
  const c = useStyles();

  let [todos, setTodos] = React.useState([]);

  let [todosPinned, setTodosPinned] = Hooks.useLocalStorage('todosPinned',[]);
  todos = todos.sort((a,b) => todosPinned.includes(a._id)?todosPinned.includes(b._id)?0:-1:1)



  useEffect(() => {
    (async () => {
      await Actions.addFakeTodos()
      await handleGetTodos()
    })()
  },[])


  const handleGetTodos = async () => {
    let newTodos = await Actions.getTodos()
    console.log('handleGetTodos',newTodos)
    // newTodos = newTodos.reverse();
    setTodos(newTodos)
  }



  return (
    <Container maxWidth="md" className={c.root}>


      <div className={c.h1}>
        My orders
      </div>



      <Paper>
        <Table className={c.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Order</TableCell>
              <TableCell align="right">Calories</TableCell>
              <TableCell align="right">Fat&nbsp;(g)</TableCell>
              <TableCell align="right">Carbs&nbsp;(g)</TableCell>
              <TableCell align="right">Protein&nbsp;(g)</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {todos.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.desc}</TableCell>
              <TableCell align="right"> </TableCell>
              <TableCell align="right"> </TableCell>
              <TableCell align="right"> </TableCell>
            </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>


    </Container>
  );
}

export default PageOrders;
