import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {todos as TODOS} from '../../data/index';
import * as Actions from '../../actions';
import * as Hooks from '../../hooks';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import Chip from '@material-ui/core/Chip';
import Button from "@material-ui/core/Button";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import AddIcon from '@material-ui/icons/Add';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';


const imageProduct = 'https://images.unsplash.com/photo-1551978129-b73f45d132eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1004&q=80';


const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 70,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  grid: {
    flexGrow: 1,
    margin: 10,
  },
  paper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: 320,
    width: 260,
  },
  textArea: {
    width:'calc(100% - 20px)',
    padding: 10,
  },
  containerSearch: {
    // marginTop: 20,
  },
  containerLabels: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  containerFilter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  containerFilterLabel: {
    marginRight: 10,
  },
  containerBtns: {
    position: 'relative',
  },
  containerRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerColumn: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    marginTop: 10,
    minWidth: 120,
    maxWidth: 300,
  },
  btn: {
    margin: 2,
    boxShadow: 'none',
    // backgroundColor: '#ff8f63',
    // '&:hover': {
    //   backgroundColor: '#ff8f63',
    // }
  },
  btnIcon: {
    width: 30,
    marginLeft: 5,
    marginRight: 5,
  },
  btnFav: {
    width: 40,
    minWidth: 40,
    position: 'absolute',
    right: 0,
  },
  label: {
    margin: 2,
  },
  productImage: {
    height: 140,
    width: '100%',
    objectFit: 'cover',
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  productDesc: {
    height: 40,
    marginLeft: 20,
    marginRight: 20,
    color: '#777777',
  },
  productPrice: {
    marginLeft: 20,
    marginRight: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  productAddCounter: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
}));





function PageMain() {
  const c = useStyles();

  let [todos, setTodos] = React.useState([]);
  const [focusTodo, setFocusTodo] = React.useState();
  const [search, setSearch] = React.useState('');
  const [searchTags, setSearchTags] = React.useState([]);
  const [newEditTodoTag, setNewEditTodoTag] = React.useState('');
  const [newTagFilter, setNewTagFilter] = React.useState('');
  const [editTodoText, setEditTodoText] = React.useState('');

  const [newTodoText, setNewTodoText] = Hooks.useLocalStorage('newTodoText','');
  const [newTodoTag, setNewTodoTag] = Hooks.useLocalStorage('newTodoTag','');
  const [newTodoTags, setNewTodoTags] = Hooks.useLocalStorage('newTodoTags',[]);
  let [todosPinned, setTodosPinned] = Hooks.useLocalStorage('todosPinned',[]);
  let [pizzasAdded, setPizzasAdded] = Hooks.useLocalStorage('pizzasAdded',[]);
  let [pizzasAddedGlobal, setPizzasAddedGlobal] = Hooks.useGlobal((s) => s.pizzasAdded,(a) => a.setState);
  todos = todos.sort((a,b) => todosPinned.includes(a._id)?todosPinned.includes(b._id)?0:-1:1)



  useEffect(() => {
    (async () => {
      await Actions.addFakeTodos()
      await handleGetTodos()
    })()
  },[])


  const handleGetTodos = async () => {
    let newTodos = await Actions.getTodos()
    console.log('handleGetTodos',newTodos)
    // newTodos = newTodos.reverse();
    setTodos(newTodos)
  }

  const handlePin = async (_id) => {
    let newTodosPinned = [];
    if (todosPinned.includes(_id)) {
      newTodosPinned = todosPinned.filter((item) => item!==_id);
    } else {
      newTodosPinned = [...todosPinned,...[_id]]
    }
    setTodosPinned(newTodosPinned);
  };

  const handleAdd = async (_id,count) => {
    if (!pizzasAddedGlobal[_id]) pizzasAddedGlobal[_id] = 0;
    pizzasAddedGlobal[_id]+=count;
    setPizzasAddedGlobal({pizzasAdded:{...pizzasAddedGlobal}});
    console.log(pizzasAddedGlobal)
  };

  const handleSearchTags = (tag) => {
    let newSearchTags = [];
    if (searchTags.includes(tag)) {
      newSearchTags = searchTags.filter((item) => item!==tag);
    } else {
      newSearchTags = [...searchTags,...[tag]]
    }
    console.log(newSearchTags)
    setSearchTags(newSearchTags);
  };



  const handleEditTodoText = async (_id,todo) => {
    todo.text = editTodoText;
    let result = await Actions.editTodo(_id,todo);
    console.log('handleEditTodoText',result);
    await handleGetTodos();
  };


  const handleEditTodoAddTag = async (_id,todo,tag) => {
    todo.tags.push(tag);
    let result = await Actions.editTodo(_id,todo);
    console.log('handleEditTodoText',result);
    await handleGetTodos();
  };

  const handleEditTodoDelTag = async (_id,todo,tag) => {
    todo.tags = todo.tags.filter((item) => item!==tag);
    let result = await Actions.editTodo(_id,todo);
    console.log('handleEditTodoText',result);
    await handleGetTodos();
  };




  return (
    <Container maxWidth="md" className={c.root}>


      <Grid
      className={c.containerSearch}
      container
      spacing={1}
      alignItems="flex-end"
      justify="center"
      >
        <Grid item>
          <SearchIcon />
        </Grid>
        <Grid item>
          <TextField
          id="search"
          label="Search"
          onChange={(e) => setSearch(e.target.value)}
          value={search}
          />
        </Grid>
      </Grid>



      <div>
        <div className={c.containerFilter}>
          <div className={c.containerFilterLabel}>
            Filter by tags:
          </div>
          <TextField
          id="search"
          onChange={(e) => setNewTagFilter(e.target.value)}
          value={newTagFilter}
          />
          <AddIcon
          fontSize="small"
          onClick={() => {
            handleSearchTags(newTagFilter)
            setNewTagFilter('')
          }}
          />
        </div>

        { searchTags.length>0 &&
        <div className={c.containerLabels}>
          {searchTags.map((tag,itag) => {
            return (
            <Chip
            key={`tag-${itag}`}
            className={c.label}
            variant="outlined"
            size="small"
            color="secondary"
            label={tag}
            onClick={() => {}}
            onDelete={() => handleSearchTags(tag)}
            />
            )
          })}
        </div>
        }
      </div>





      <Grid container className={c.grid} spacing={3}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={3}>



            { todos.map((todo,it) => {
              let {_id, name, tags, image, desc, price} = todo;
              if (searchTags.length>0 && tags.filter(value => searchTags.includes(value)).length===0) return;
              if (!name.toLowerCase().includes(search.toLowerCase())) return;
              const pinned = todosPinned.includes(_id);
              return (
                <Grid key={`todo-${it}`} item>
                  <Paper className={c.paper} elevation={6}>

                    <div>
                      <img
                      className={c.productImage}
                      src={image ? image : imageProduct}
                      />
                    </div>

                    { focusTodo !== _id &&
                      <div
                      className={c.productName}
                      onClick={() => {
                        setEditTodoText(name)
                        setFocusTodo(_id)
                      }}
                      >
                        {name}
                      </div>
                    }


                    <div
                    className={c.productDesc}
                    >
                      {desc && desc}
                    </div>


                    { focusTodo===_id &&
                    <TextareaAutosize
                    className={c.textArea}
                    rowsMin={9}
                    placeholder="Type text here..."
                    value={editTodoText}
                    onChange={(e) => setEditTodoText(e.target.value)}
                    onBlur={() => {
                      handleEditTodoText(_id,todo)
                      setFocusTodo(null)
                    }}
                    />
                    }



                    {focusTodo === _id &&
                    <div className={c.containerFilter}>
                      <div className={c.containerFilterLabel}>
                        Tags:
                      </div>
                      <TextField
                      id="search"
                      onChange={(e) => setNewEditTodoTag(e.target.value)}
                      value={newEditTodoTag}
                      />
                      <AddIcon
                      fontSize="small"
                      onClick={() => {
                        handleEditTodoAddTag(_id,todo,newEditTodoTag)
                        setNewEditTodoTag('')
                      }}
                      />
                    </div>
                    }

                    <div className={c.containerLabels}>
                      { tags.map((tag,itag) => {
                        let fHandleSearchTags = () => handleEditTodoDelTag(_id,todo,tag);
                        let onDelete = focusTodo === _id ? fHandleSearchTags : null;
                        return (
                        <Chip
                        key={`tag-${itag}`}
                        className={c.label}
                        variant="outlined"
                        size="small"
                        color="secondary"
                        label={tag}
                        onClick={() => handleSearchTags(tag)}
                        onDelete={onDelete}
                        />
                        )
                      })}
                    </div>

                    <div className={c.containerRow}>
                      { pizzasAddedGlobal ? !pizzasAddedGlobal[_id] ?
                      <Button
                      className={c.btn}
                      variant="contained"
                      color="secondary"
                      elevation={0}
                      onClick={() => handleAdd(_id,1)}
                      >
                        To cart
                      </Button>
                      :
                      <div className={c.containerRow}>
                        <Button
                        className={c.btn}
                        variant="contained"
                        color="secondary"
                        elevation={0}
                        onClick={() => handleAdd(_id,-1)}
                        >
                          -
                        </Button>
                        <div className={c.productAddCounter}>
                          {pizzasAddedGlobal[_id] && pizzasAddedGlobal[_id]}
                        </div>
                        <Button
                        className={c.btn}
                        variant="contained"
                        color="secondary"
                        elevation={0}
                        onClick={() => handleAdd(_id,1)}
                        >
                          +
                        </Button>
                      </div>
                      : null
                      }
                      { price &&
                      <div className={c.productPrice}>
                        {price.eu && price.eu}{' '}€
                      </div>
                      }
                    </div>


                    <Button
                    className={c.btnFav}
                    color="secondary"
                    onClick={() => handlePin(_id)}
                    >
                      { pinned ?
                      <StarIcon
                      fontSize="small"
                      />
                      :
                      <StarBorderIcon
                      fontSize="small"
                      />
                      }
                    </Button>

                  </Paper>
                </Grid>
              )
            })}
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}

export default PageMain;
