import React, {useEffect} from 'react';
import {Switch, Route, Link} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import * as Hooks from "../../hooks";
import * as Actions from "../../actions";


const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    top: 0,
    backgroundColor: 'white',
    boxShadow: '0 0 10px lightgrey',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: '100%',
    zIndex: 999,
  },
  logo: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 15,
    marginRight: 15,
    textAlign: 'center',
    lineHeight: '50px',
    textDecoration: 'none',
    color: '#000',
  },
  item: {
    fontSize: 20,
    marginLeft: 15,
    marginRight: 15,
    textAlign: 'center',
    lineHeight: '50px',
    textDecoration: 'none',
    color: '#000',
    cursor: 'pointer',
  },
  hr: {
    width: 1,
    height: '70%',
    background: 'linear-gradient(#e66465, #9198e5)'
  },
}));


function MenuMain() {
  const c = useStyles();
  const [logged, setLogged] = React.useState(false);
  let [pizzasAddedGlobal, setPizzasAddedGlobal] = Hooks.useGlobal((s) => s.pizzasAdded,(a) => a.setState);
  let countPizzasAdded = 0;
  Object.keys(pizzasAddedGlobal).map((_id) => countPizzasAdded+=pizzasAddedGlobal[_id])


  useEffect(()=>{
    (async () => {
      await handleLogged()
    })()
    setInterval(handleLogged,2000);
  },[])


  const handleLogout = async () => {
    const result = await Actions.logout()
    if (result.status===200) {
      setLogged(false)
    }
  }



  const handleLogged = async () => {
    const result = await Actions.isLogged()
    setLogged(result.data)
  }


  return (
    <div className={c.root}>
      <Link
      className={c.logo}
      to="/"
      >
        Pizza Delivery
      </Link>
      <div className={c.hr}/>
      <Link
      className={c.item}
      to="/cart"
      >
        <Badge
        badgeContent={countPizzasAdded}
        color="secondary"
        >
          <ShoppingBasketIcon />
        </Badge>
      </Link>

      <div className={c.hr}/>

      { logged ?
      <Link
      className={c.item}
      to="/orders"
      >
        My orders
      </Link>
      :
      <Link
      className={c.item}
      to="/sign/login"
      >
        Log In
      </Link>
      }


      { logged &&
      <>
        <div className={c.hr}/>
        <div
        className={c.item}
        onClick={handleLogout}
        >
          Log Out
        </div>
      </>
      }

    </div>
  );
}

export default MenuMain;
