const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
  email : {
    type: String,
    unique: true,
    required: true
  },
  password : {
    type: String,
    required: true
  }
})


UserSchema.methods.generateHash = (password) => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

UserSchema.methods.validPassword = function (password) {
  // should be NOT arrow function to use "this"!
  return bcrypt.compareSync(password, this.password);
};



module.exports = mongoose.model('User', UserSchema);