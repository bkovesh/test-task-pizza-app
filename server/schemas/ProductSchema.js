const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const ProductSchema = new Schema({
  name: String,
  desc: String,
  image: String,
  price: {},
  tags: [String],
  ingredients: [String],
});


module.exports = mongoose.model('Product', ProductSchema);