module.exports = [
  {
    name: 'Mexicana',
    desc: 'Delicious hot pizza in mexican style.',
    price: {
      us: 10,
      eu: 8,
    },
    ingredients: [
      'Meat',
      'Tomatoes',
      'Chili',
      'Jalapeno',
    ],
    tags: [
      'Hot',
      'Meat',
    ],
    image: 'https://images.unsplash.com/photo-1551978129-b73f45d132eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1004&q=80'
  },
  {
    name: 'Vegetarian',
    tags: [
      'Veggi'
    ]
  },
  {
    name: 'Carbonara',
    tags: [
      'Meat'
    ]
  },
  {
    name: 'Margareta',
    tags: [],
    image: 'https://images.unsplash.com/photo-1581873372796-635b67ca2008?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
  },
  {
    name: 'Hawaii',
    tags: [
      'Seafood'
    ]
  },
  {
    name: 'Visllage',
    tags: [
      'Meat'
    ]
  },
]

