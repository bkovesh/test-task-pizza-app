var express = require('express');
var router = express.Router();
const Product = require('../schemas/ProductSchema');
const data = require('../data');


router.get('/', async (req, res, next) => {
  try {
    const products = await Product.find({})
    console.log(products)
    res.status(200).send({data:products})
  } catch (e) {
    console.error(e);
    res.status(403).send('Server error')
  }
});


router.get('/add-fake-data', async (req, res, next) => {
  try {
    const result = await Product.insertMany(data)
    console.log(result)
    res.status(200).send({msg:'Done'})
  } catch (e) {
    console.error(e);
    res.status(403).send('Server error')
  }
});


router.get('/drop', async (req, res, next) => {
  try {
    const result = await Product.collection.drop()
    console.log(result)
    res.status(200).send({msg:'Done'})
  } catch (e) {
    console.error(e);
    res.status(403).send('Server error')
  }
});



module.exports = router;
