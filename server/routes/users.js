const express = require('express');
const router = express.Router();
const User = require('../schemas/UserSchema');


router.get('/', async (req, res, next) => {
  try {
    const models = User.find({})
    res.status(200).send(models);
  } catch (e) {
    console.error(e);
    res.status(403).send('Server error');
  }
});


router.post('/reg', async (req, res) => {
  try {
    console.log("reg",req.body)
    let {email,password} = req.body;
    email = email.toLowerCase();
    const user = await User.findOne({email});
    console.log(user)
    if (user) return res.status(403).send({msg:'Email already exists!'});
    let newUser = new User(req.body);
    newUser.password = newUser.generateHash(password);
    const success = await newUser.save();
    if (!success) return res.status(403).send('Database error')
    req.userId = newUser._id;
    req.session.save();
    return res.status(200).send(req.session)
  } catch (e) {
    console.error(e);
    res.status(403).send('Server error');
  }
});



router.post('/login', async (req, res, next) => {
  try {
    let {email,password} = req.body;
    if (req.userId) return res.status(403).send({msg:"You are already logged in"})
    const user = await User.findOne({email}).exec();
    if(!user) return res.status(403).send({msg:"Not logged"});
    if (user && !user.validPassword(password)) return res.status(403).send({msg:"Not logged"});
    req.session.userId = user._id;
    // req.session.save();
    return res.status(200).send(req.session);
  } catch (e) {
    console.error(e);
    return res.status(403).send({msg:'Server error'});
  }
});



router.post('/logout', async (req, res, next) => {
  try {
    if (req.userId) {
      delete req.userId;
      req.session.destroy();
      return res.status(200).send({msg:'Logged out'});
    }
    return res.status(200).send({msg:'You are not logged already'});
  } catch (e) {
    console.error(e);
    return res.status(403).send({msg:'Server error'});
  }
});


router.post('/islogged', async (req, res, next) => {
  console.log('isLogged',req.session,req.user,req.headers.cookie,req.cookies)
  try {
    if (req.userId) return res.status(200).send({data:true,msg:"You are logged in"})
    return res.status(200).send({data:false,msg:'You are NOT logged'});
  } catch (e) {
    console.error(e);
    return res.status(403).send({data:false,msg:'Server error'});
  }
});


module.exports = router;
