const createError = require('http-errors');
const express = require('express');
const http = require('http');
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const session = require('express-session')
const MongoStore = require('connect-mongo')(session);
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const productsRouter = require('./routes/products');
require('dotenv').config();

const isDev = process.env.NODE_ENV!=='production';
const DB_URL = process.env.DB_URL;
const CLIENT_URL = isDev ? 'http://localhost:3000' : 'https://test-task-pizza.netlify.app';
const CLIENT_DOMAIN = isDev ? 'localhost' : '.netlify.app';
const SERVER_URL = isDev ? 'http://localhost:5000' : 'https://test-task-pizza.herokuapp.com';


const app = express();
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json())
const corsOptions = {
  origin: CLIENT_URL,
  credentials: true,
}
// app.use(cors(corsOptions))
// app.set('trust proxy', 1)
app.enable('trust proxy')
const store = new MongoStore({
  url: DB_URL,
  collection: 'sessions',
  autoReconnect: true,
  expires: 1000 * 60 * 60 * 24 * 1, // days
})

app.use(session({
  cookie: {
    path: '/',
    secure: isDev ? false : true,
    domain: CLIENT_DOMAIN,
    sameSite: isDev ? false : 'None',
    // maxAge: 1000*60*60*24*1, // days // should be removed, because^ https://stackoverflow.com/questions/40324121/express-session-secure-true/40324493#40324493
  },
  secret: 'my pizza app',
  resave: false,
  saveUninitialized: false,
  rolling: true,
  proxy : true,
  store
}))

store.on('error', (e) => console.log('MongoSessionStore err',e));


app.use(function(req, res, next) {
  if (req.session && req.session.userId) {
    req.userId = req.session.userId;
    next();
  } else {
    next();
  }
});


app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', CLIENT_URL);
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-AUTHENTICATION, X-IP, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

// ================================



app.post('/login', (req, res) => {
  console.log('**testing**')
  console.log(req.session)
  console.log(req.cookies)
  return res.status(200).send(req.session)
})

app.use('/users', usersRouter);
app.use('/products', productsRouter);

// ================================


app.use(function(req, res, next) {
  next(createError(404));
});


app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.send('error');
});



const server = http.createServer(app);

const dbOpts = {
  autoIndex: false,
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}

mongoose.connect(DB_URL, dbOpts, (err) => {
  if(err) return console.log(err);
  server.listen(isDev ? 5000 : process.env.PORT);
  server.on('error', (e) => console.error(e));
  server.on('listening', (e) => console.log('Server is listening'));
});

module.exports = app;
